---
# This schema defines the expected layout and format of every object in the
# info/owners.yaml file found in this project.

# Documentation for the schema specification itself can be found here:
# https://json-schema.org/understanding-json-schema/reference


# Valid SST names for the `devel-sst` property.
sst_names: &sst_names
  - rhel-sst-accelerators
  - rhel-ssg-arr
  - rhel-sst-arch-hw
  - rhel-sst-cee-supportability
  - rhel-sst-centos-stream
  - rhel-sst-cki
  - rhel-sst-cloudexperience
  - rhel-sst-cockpit
  - rhel-sst-container-tools
  - rhel-sst-cpe
  - rhel-sst-cpt
  - rhel-sst-cs-apps
  - rhel-sst-cs-infra-services
  - rhel-sst-cs-plumbers
  - rhel-sst-cs-software-management
  - rhel-sst-cs-system-management
  - rhel-sst-desktop
  - rhel-sst-desktop-applications
  - rhel-sst-desktop-firmware-bootloaders
  - rhel-sst-desktop-platform-technologies
  - rhel-sst-edge
  - rhel-sst-filesystems
  - rhel-sst-gpu
  - rhel-sst-high-availability
  - rhel-sst-i18n
  - rhel-sst-idm-cs
  - rhel-sst-idm-ds
  - rhel-sst-idm-hms
  - rhel-sst-idm-ipa
  - rhel-sst-idm-ops
  - rhel-sst-idm-sssd
  - rhel-sst-image-builder
  - rhel-sst-installer
  - rhel-sst-kernel-debug
  - rhel-sst-kernel-ft
  - rhel-sst-kernel-maintainers
  - rhel-sst-kernel-rts
  - rhel-sst-kernel-security
  - rhel-sst-kernel-tps
  - rhel-sst-kernel-tps-gitlab
  - rhel-sst-logical-storage
  - rhel-sst-network-drivers
  - rhel-sst-network-fastdatapath
  - rhel-sst-networking-core
  - rhel-sst-osci
  - rhel-sst-packit
  - rhel-sst-pt-gcc-glibc
  - rhel-sst-pt-llvm-rust-go
  - rhel-sst-pt-pcp
  - rhel-sst-pt-perf-debug
  - rhel-sst-rh-ceph-storage
  - rhel-sst-rhcos
  - rhel-sst-security-compliance
  - rhel-sst-security-crypto
  - rhel-sst-security-readiness
  - rhel-sst-security-selinux
  - rhel-sst-security-special-projects
  - rhel-sst-storage-io
  - rhel-sst-storage-management
  - rhel-sst-subscription-manager
  - rhel-sst-system-roles
  - rhel-sst-upgrades
  - rhel-sst-virtualization
  - rhel-sst-virtualization-advanced
  - rhel-sst-virtualization-cloud
  - rhel-sst-virtualization-hwe
  - rhel-sst-virtualization-networking
  - rhel-sst-virtualization-spice
  - rhel-sst-virtualization-storage
  - rhel-sst-virtualization-windows
  - rhivos-ft-auto-kernel
  - rhivos-ft-cats
  - RHEL-SST-Conversions
  - security-compliance-team

# Valid kernel variants for the `testVariants` property.
variants: &variants
  - kernel-rt
  - kernel-64k
  - kernel-automotive

user: &user
  title: user
  description: A Gitlab user
  type: object
  properties:
    name:
      description: Name of the user.
      type: string
    email:
      description: Email address of the user.
      type: string
      format: email
    gluser:
      description: Gitlab username of the user.
      type: string
    restricted:
      description: Whether to exclude the user from the list of required reviewers.
      type: boolean
      default: false
  required:
    - name
    - email
    - gluser
  additionalProperties: false


subsystem: &subsystem
  title: subsystem
  description: A kernel subsystem.
  type: object
  properties:
    subsystem:
      type: string
      description: Brief description of the subsystem.
    labels:
      description: Labels added by kernel-webhooks to MRs that are relevant to this subsystem.
      type: object
      properties:
        name:
          description: Brief name of the subsystem, used to derive `Subsystem:{name}` label and approval rule name.
          type: string
        readyForMergeDeps:
          description: List of `ExternalCI::` labels that will be assigned to relevant MRs.
          type: array
          items:
            type: string
          default: []
      required:
        - name
      additionalProperties: false
    status:
      description: Red Hat support status of this subsystem.
      type: string
      enum:
        - Supported
        - Provided
        - Internal
        - Disabled
        - Unassigned
    devel-sst:
      description: List of Red Hat Engineering SSTs that are responsible for developing this subsystem.
      type: array
      items:
        type: string
        enum: *sst_names
        message:
          enum: The specified SST is not in the list of known SSTs. If this is a new SST (and not just a typo), please adjust owners-schema.yaml.
      minItems: 1
      uniqueItems: true
    jiraComponent:
      description: RHEL jira project component that should be set when filing issues against this subsystem.
      type: string
    requiredApproval:
      description: Whether a relevant MR must get an approval from a maintainer or a reviewer of this subsystem before it can be merged.
      type: boolean
      default: false
    maintainers:
      description: List of maintainers for this subsystem. The developers listed here are the primary contact for the subsystem. Must have at least one.
      type: array
      items: *user
      minItems: 1
      uniqueItems: true
    reviewers:
      description: List of additional reviewers for this subsystem.
      type: array
      items: *user
      minItems: 0
      uniqueItems: true
    paths:
      description: Paths matched to identify MRs relevant to this subsystem. Although optional, in practice, at least one path should be specified.
      type: object
      properties:
        includes:
          description: List of files and directories to include. Can contain shell-like wildcard patterns.
          type: array
          items:
            type: string
        includeRegexes:
          description: List of files and directories regex patterns to include.
          type: array
          items:
            type: string
        excludes:
          description: List of files and directories to exclude. Can contain shell-like wildcard patterns.
          type: array
          items:
            type: string
      additionalProperties: false
    scm:
      description: SCM tree type and location string. Type is one of `git`, `hg`, `quilt`, `stgit`. The type and location is separated by a space.
      type: string
      pattern: "^(git|hg|quilt|stgit) https://.*$"
      default: ""
    mailingList:
      description: Mailing list for the subsystem.
      type: string
      default: ""
    testVariants:
      description: List of kernel variants that should be tested when a file in `paths` is changed for this subsystem.
      type: array
      items:
        type: string
        enum: *variants
        message:
          enum: Unknown kernel variant specified. If this is a new variant (and not just a typo), please adjust owners-schema.yaml.
      minItems: 0
      uniqueItems: true
  required:
    - subsystem
    - labels
    - status
    - devel-sst
    - jiraComponent
    - maintainers
  additionalProperties: false


$schema: https://json-schema.org/draft/2020-12/schema
$id: https://gitlab.com/redhat/centos-stream/src/kernel/documentation/-/raw/main/owners-schema.yaml
title: Schema for owners.yaml file
description: Properties of the CentOS Stream documentation project owners.yaml file.
type: object
properties:
  subsystems:
    description: List of kernel Sub System Teams (SSTs).
    type: array
    items: *subsystem
    minItems: 1
    uniqueItems: true
required:
  - subsystems
additionalProperties: false
